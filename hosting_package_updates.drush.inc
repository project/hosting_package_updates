<?php

/**
 * implementation of the hosting_post_hosting_package_updates_task hook
 */
function hosting_package_updates_post_hosting_hosting_package_updates_task($task, $data) {
  if ($task->ref->type == 'platform') {

    db_query('DELETE FROM {hosting_package_updates} WHERE platform_id = %d', array($task->ref->nid));

    $result = db_query('SELECT hpi.rid, hp.short_name FROM {hosting_package_instance} hpi JOIN {hosting_package} hp ON hp.nid = hpi.package_id WHERE hpi.platform = %d GROUP BY hp.short_name', array($task->ref->nid));

    $package_instance_ids = array();
    while ($row = db_fetch_array($result)) {
      $package_instance_ids[$row['short_name']] = $row['iid'];
    }

    if (!empty($data['context']['package_updates'])) {
      foreach ($data['context']['package_updates'] as $projectname => $update_info) {
        $record = array(
          'platform_id' => $task->ref->nid,
          'hpi_id' => $package_instance_ids[$projectname],
          'package_short_name' => $projectname,
          'number_of_noncritical_updates' => $update_info['noncritical_updates'],
          'number_of_critical_updates' => $update_info['critical_updates'],
        );
        drupal_write_record('hosting_package_updates', $record);
      }
    }
  }
}
