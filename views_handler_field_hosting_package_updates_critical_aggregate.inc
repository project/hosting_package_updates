<?php

/**
 * A handler to provide a count of noncritical updates on a platform.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_hosting_package_updates_critical_aggregate extends views_handler_field {
  function render($values) {
    return db_result(db_query('SELECT SUM(number_of_critical_updates) FROM {hosting_package_updates} WHERE platform_id = %d', array($values->hosting_platform_nid)));
  }
}
