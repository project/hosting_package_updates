<?php

/**
 * Implementation of hook_views_handlers().
 */
function hosting_package_updates_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'hosting_package_updates'),
      ),
    'handlers' => array(
      'views_handler_field_hosting_package_updates_noncritical_aggregate' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_field_hosting_package_updates_critical_aggregate' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_default_views_alter().
 */
function hosting_package_updates_views_default_views_alter(&$views) {
  if (isset($views['hosting_platform_list'])) {
    /* Field: Hosting Package Updates: Total critical updates */
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['id'] = 'aggregate_critical_updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['table'] = 'hosting_package_updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['field'] = 'aggregate_critical_updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['label'] = 'Critical updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['alter_text'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['make_link'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['absolute'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['external'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['replace_spaces'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['trim_whitespace'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['nl2br'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['word_boundary'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['ellipsis'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['strip_tags'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['trim'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['alter']['html'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['element_label_colon'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['element_default_classes'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['hide_empty'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['empty_zero'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_critical_updates']['hide_alter_empty'] = 1;
    /* Field: Hosting Package Updates: Total noncritical updates */
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['id'] = 'aggregate_noncritical_updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['table'] = 'hosting_package_updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['field'] = 'aggregate_noncritical_updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['label'] = 'Noncritical updates';
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['alter_text'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['make_link'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['absolute'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['external'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['replace_spaces'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['trim_whitespace'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['nl2br'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['word_boundary'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['ellipsis'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['strip_tags'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['trim'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['alter']['html'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['element_label_colon'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['element_default_classes'] = 1;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['hide_empty'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['empty_zero'] = 0;
    $views['hosting_platform_list']->display['default']->display_options['fields']['aggregate_noncritical_updates']['hide_alter_empty'] = 1;
  }
}


/**
 * Implementation of hook_views_data().
 */
function hosting_package_updates_views_data() {
  $data['hosting_package_updates']['table'] = array(
    'group' => 'Hosting Package Updates',
    'title' => 'Package Updates',
    'join' => array(
      'hosting_package_instance' => array(
        'left_field' => 'iid',
        'field' => 'hpi_id',
      ),
      'node' => array(
        'left_field' => 'nid',
        'field' => 'platform_id',
      ),
    ),
  );

  $data['hosting_package_updates']['aggregate_noncritical_updates'] = array(
    'title' => t('Total noncritical updates'),
    'help' => t('A count of total noncritical updates for a platform.'),
    'real field' => 'platform_id',
    'field' => array(
      'handler' => 'views_handler_field_hosting_package_updates_noncritical_aggregate',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['hosting_package_updates']['aggregate_critical_updates'] = array(
    'title' => t('Total critical updates'),
    'help' => t('A count of total critical updates for a platform.'),
    'real field' => 'platform_id',
    'field' => array(
      'handler' => 'views_handler_field_hosting_package_updates_critical_aggregate',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}
