<?php

/**
 * Implementation of hook_hosting_feature().
 */
function hosting_package_updates_hosting_feature() {

  $features['hosting_package_updates'] = array(
    'title' => t('Hosting package updates'),
    'description' => t("Automated package update checks for platforms."),
    'status' => HOSTING_FEATURE_ENABLED,
    'module' => 'hosting_package_updates',
  );

  return $features;
}
